// uploader module
var fs = require('fs');
var uuid = require('uuid');
var request = require('request');

function Upload(client){

  this.client = client;

  function upload(args){
    if( args.srcPath.indexOf('http://')!==-1 ){
      var readStream = request.get(args.srcPath).pipe(require('stream').PassThrough());
    } else {
      var readStream = fs.createReadStream(args.srcPath);
    }

    var writeStream = client.upload({
      container: args.container,
      remote: args.destPath || uuid.v4()
    });

    writeStream.on('error', function(err) {
      args.cb(err, null);
    });

    writeStream.on('success', function(file) {
     args.cb(null, file);
    });

    readStream.pipe(writeStream);
  }

  return upload;

}



module.exports = Upload;
