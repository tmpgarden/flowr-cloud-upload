module.exports = function(client){
  var cloud = {
    upload: require('./uploader.js')(client)
  };

  return cloud;
};
