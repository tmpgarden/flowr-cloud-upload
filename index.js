var Hose = require('flowr-hose');
var pkgcloud = require('pkgcloud');

var hose = new Hose({
  name: 'cloud',
  port: process.env.PORT || 3000,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("CloudUpload is running...");

hose.process('upload', function(job, done){
	console.log("UPLOAD job arrived!");
	upload(job.data, function(err, file){
    if(err) {
		  console.log(err);
      done(err, null);
    } else {
      console.log(file);
		  done(null, file);
    }
	});
});


var clouds = {};

function upload(data, done){

  if(!clouds[data.cloud.username]){

    var client = pkgcloud.storage.createClient({
      provider: data.cloud.provider, // required
      username: data.cloud.username, // required
      password: data.cloud.password,
      version: data.cloud.version || 'v2.0',
      region: data.cloud.region,
      authUrl: data.cloud.authUrl,
    });

    clouds[data.cloud.username] = new require('./lib')(client);
  }

  var cloud = clouds[data.cloud.username];

  cloud.upload({
    container: data.cloud.container,
    srcPath: data.srcPath,
    destPath: data.destPath,
    cb: function(err, file) {
      if(err) {
        done(err, null);
      } else {
        done(null, file);
      }
    }
  });

}
